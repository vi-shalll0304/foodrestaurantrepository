package com.orderservice.dto;

import lombok.Data;

@Data
public class UserRequestDto {
	private String username;
	private String password;
}
